import request from "@/utils/request";

// 活动订单列表
export function getMemberList(query) {
  return request({
    url: "/coupon/member/list",
    method: "get",
    params: query,
  });
}

// 活动订单详情
export function getMemberById(query) {
  return request({
    url: `/coupon/member/${query.id}`,
    method: "get",
  });
}
// 活动订单导出
export function exportMember(query) {
  return request({
    url: `/coupon/member/export`,
    method: "get",
    params: query,
  });
}
