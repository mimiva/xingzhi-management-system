import request from "@/utils/request";

// 获取优惠券列表
export function getCouponList(query) {
  return request({
    url: "/coupon/coupon/list",
    method: "get",
    params: query,
  });
}

// 新增优惠券
export function addCoupon(data) {
  return request({
    url: "/coupon/coupon",
    method: "post",
    data: data,
  });
}

// 修改优惠券
export function putCoupon(data) {
  return request({
    url: "/coupon/coupon",
    method: "put",
    data: data,
  });
}

// 查询优惠券
export function getCouponById(query) {
  return request({
    url: `/coupon/coupon/${query.id}`,
    method: "get",
  });
}

// 删除优惠券
export function deleteCouponById(query) {
  return request({
    url: `/coupon/coupon/${query.id}`,
    method: "delete",
  });
}

// 上下架优惠券
export function suspendedCoupon(data) {
  return request({
    url: "/coupon/coupon/suspendedeCoupon",
    method: "put",
    params: data,
  });
}

// 导出优惠券
export function exportCoupon(data) {
  return request({
    url: "/coupon/coupon/export",
    method: "get",
    params: data,
  });
}

// 发送指定业务
export function sendBusiness(data) {
  return request({
    url: "/coupon/business",
    method: "post",
    data: data,
  });
}

// 获取业务类型
export function businessList(query) {
  return request({
    url: "/coupon/coupon/businessList",
    method: "get",
    params: query,
  });
}

// 获取指定项目编号
export function serviceItemsList(query) {
  return request({
    url: "/coupon/coupon/serviceItemsList",
    method: "get",
    params: query,
  });
}

// 获取适用单位
export function applicableUnitList(query) {
  return request({
    url: "/coupon/coupon/applicableUnitList",
    method: "get",
    params: query,
  });
}

// 获取发送节点
export function distributedNodeList() {
  return request({
    url: "/coupon/coupon/distributedNodeList",
    method: "get",
  });
}
