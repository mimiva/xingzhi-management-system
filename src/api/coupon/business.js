import request from "@/utils/request";

// 优惠券业务列表
export function getBusinessList(query) {
  return request({
    url: "/coupon/business/list",
    method: "get",
    params: query,
  });
}

// 优惠券指定业务
export function appointBusiness(data) {
  return request({
    url: "/coupon/business",
    method: "post",
    data: data,
  });
}

// 编辑优惠券指定业务
export function putBusiness(data) {
  return request({
    url: "/coupon/business",
    method: "put",
    data: data,
  });
}

// 优惠券业务列详情
export function getBusinessById(query) {
  return request({
    url: `/coupon/business/${query.id}`,
    method: "get",
  });
}

// 优惠券指定业务删除
export function deleteBusinessById(query) {
  return request({
    url: `/coupon/business/${query.id}`,
    method: "delete",
  });
}

// 发送指定客户
export function memberBusiness(data) {
  return request({
    url: "/coupon/member",
    method: "post",
    data: data,
  });
}

// 导出优惠券
export function exportBusiness(query) {
  return request({
    url: "/coupon/business/export",
    method: "get",
    params: query,
  });
}

// 优惠券数据统计
export function businessDataInfo(query) {
  return request({
    url: `/coupon/business/businessDataInfo/${query.id}`,
    method: "get",
  });
}

// 客户领取优惠券记录
export function businessMemberDataList(query) {
  return request({
    url: "/coupon/business/businessMemberDataList",
    method: "get",
    params: query,
  });
}

// 获取客户列表
export function memberList(query) {
  return request({
    url: "/coupon/business/memberList",
    method: "get",
    params: query,
  });
}
