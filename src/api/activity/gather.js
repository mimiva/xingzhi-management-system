import request from "@/utils/request";

// 获取同行活动列表
export function getCollectList(query) {
  return request({
    url: "/activity/collect/list",
    method: "get",
    params: query,
  });
}

// 获取同行活动详情
export function getGatherById(query) {
  return request({
    url: `/activity/collect/${query.id}`,
    method: "get",
  });
}

// 新增同行活动
export function collect(data) {
  return request({
    url: `/activity/collect`,
    method: "post",
    data: data,
  });
}

// 集客活动删除
export function deleteCollectById(data) {
  return request({
    url: `/activity/collect/${data.id}`,
    method: "delete",
  });
}

// 集客活动导出
export function collectExport(query) {
  return request({
    url: `/activity/collect/export`,
    method: "get",
    params: query,
  });
}

// 开始集客活动
export function collectUpdateState(data) {
  return request({
    url: `/activity/collect/updateState`,
    method: "put",
    data: data,
  });
}

//获取活动数据
export function getCollectInfo(query) {
  return request({
    url: `/activity/records/getCollectInfo/${query.id}`,
    method: "get",
  });
}

//集客活动已参与客户列表
export function getPurchasedCustomers(query) {
  return request({
    url: `/activity/records/getParticipatingCustomers`,
    method: "get",
    params: query,
  });
}
