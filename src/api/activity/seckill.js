import request from "@/utils/request";

// 获取秒杀活动列表
export function getSeckillList(query) {
  return request({
    url: "/activity/seckill/list",
    method: "get",
    params: query,
  });
}

// 优惠券列表
export function couponBusActivityList(query) {
  return request({
    url: "/coupon/business/couponBusActivityList",
    method: "get",
    params: query,
  });
}

// 秒杀活动删除
export function deleteSeckillById(query) {
  return request({
    url: `/activity/seckill/${query.id}`,
    method: "delete",
  });
}

// 秒杀活动导出
export function exportSeckill(query) {
  return request({
    url: `/activity/seckill/export`,
    method: "get",
    params: query,
  });
}

// 秒杀活动导出
export function addSeckill(data) {
  return request({
    url: `/activity/seckill`,
    method: "post",
    data: data,
  });
}

// 获取秒杀活动列表
export function getSeckillById(query) {
  return request({
    url: `/activity/seckill/${query.id}`,
    method: "get",
  });
}

// 获取秒杀活动
export function copyActivitySeckill(query) {
  return request({
    url: `/activity/seckill/copyActivitySeckill/${query.id}`,
    method: "get",
  });
}

//开始、暂停、结束秒杀活动
export function suspendedeSeckill(data) {
  return request({
    url: "/activity/seckill/suspendedeSeckill",
    method: "put",
    params: data,
  });
}

//获取活动数据
export function getSeckillInfo(query) {
  return request({
    url: `/activity/records/getSeckillInfo/${query.id}`,
    method: "get",
  });
}

//秒杀活动已购买客户列表
export function getPurchasedCustomers(query) {
  return request({
    url: `/activity/records/getPurchasedCustomers`,
    method: "get",
    params: query,
  });
}

//浏览客户列表
export function getBrowseCustomers(query) {
  return request({
    url: `/activity/records/list`,
    method: "get",
    params: query,
  });
}
