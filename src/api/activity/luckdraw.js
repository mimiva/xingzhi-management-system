import request from "@/utils/request";

// 获取秒杀活动列表
export function getLuckdrawList(query) {
  return request({
    url: "/activity/luckdraw/list",
    method: "get",
    params: query,
  });
}

// 新增抽奖活动
export function addLuckdraw(data) {
  return request({
    url: `/activity/luckdraw`,
    method: "post",
    data: data,
  });
}

// 修改抽奖活动
export function getLuckdrawById(query) {
  return request({
    url: `/activity/luckdraw/${query.id}`,
    method: "get",
  });
}

// 删除抽奖活动
export function deleteLuckdrawById(query) {
  return request({
    url: `/activity/luckdraw/${query.id}`,
    method: "delete",
  });
}

// 更新抽奖活动
export function luckdrawUpdateState(data) {
  return request({
    url: `/activity/luckdraw/updateState`,
    method: "put",
    data: data,
  });
}

// 导出抽奖活动
export function luckdrawExport(data) {
  return request({
    url: `/activity/luckdraw/export`,
    method: "get",
    data: data,
  });
}

//抽奖活动数据
export function getLuckdrawInfo(query) {
  return request({
    url: `/activity/records/getLuckdrawInfo/${query.id}`,
    method: "get",
  });
}

//抽奖活动参与用户列表
export function getParticipatingUsers(query) {
  return request({
    url: `/activity/records/getParticipatingUsers`,
    method: "get",
    params: query,
  });
}

//抽奖活动获奖用户列表
export function getAWinningUsers(query) {
  return request({
    url: `/activity/records/getAWinningUsers`,
    method: "get",
    params: query,
  });
}
