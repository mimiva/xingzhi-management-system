import request from "@/utils/request";

// 获取同行活动列表
export function getPeerList(query) {
  return request({
    url: "/activity/peer/list",
    method: "get",
    params: query,
  });
}

// 新增同行活动
export function addPeer(data) {
  return request({
    url: "/activity/peer",
    method: "post",
    data: data,
  });
}

// 同行活动删除
export function deletePeerById(query) {
  return request({
    url: `/activity/peer/${query.id}`,
    method: "delete",
  });
}

// 同行活动导出
export function peerExport(query) {
  return request({
    url: `/activity/peer/export`,
    method: "get",
    params: query,
  });
}

// 同行活动更新状态
export function peerUpdateState(data) {
  return request({
    url: `/activity/peer/updateState`,
    method: "put",
    data: data,
  });
}

// 获取同行活动详情
export function getPeerById(query) {
  return request({
    url: `/activity/peer/${query.id}`,
    method: "get",
  });
}

//获取活动数据
export function getPeerInfo(query) {
  return request({
    url: `/activity/records/getPeerInfo/${query.id}`,
    method: "get",
  });
}

//同行活动发起客户列表
export function selectInvitedUsersList(query) {
  return request({
    url: `/activity/records/selectInvitedUsersList`,
    method: "get",
    params: query,
  });
}

//同行活动发起客户列表
export function getAwardWinningUsers(query) {
  return request({
    url: `/activity/records/getAwardWinningUsers`,
    method: "get",
    params: query,
  });
}
