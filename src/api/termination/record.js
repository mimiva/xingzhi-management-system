import request from '@/utils/request'

// 查询解约记录列表
export function listRecord(query) {
  return request({
    url: '/termination/record/list',
    method: 'get',
    params: query
  })
}

// 查询解约记录详细
export function getRecord(id) {
  return request({
    url: '/termination/record/' + id,
    method: 'get'
  })
}

// 新增解约记录
export function addRecord(data) {
  return request({
    url: '/termination/record',
    method: 'post',
    data: data
  })
}

// 修改解约记录
export function updateRecord(data) {
  return request({
    url: '/termination/record',
    method: 'put',
    data: data
  })
}

// 删除解约记录
export function delRecord(id) {
  return request({
    url: '/termination/record/' + id,
    method: 'delete'
  })
}

// 导出解约记录
export function exportRecord(query) {
  return request({
    url: '/termination/record/export',
    method: 'get',
    params: query
  })
}