import request from "@/utils/request";

// 活动流水列表接口
export function getOrderPaymentRecordList(query) {
  return request({
    url: "/order/orderPaymentRecord/list",
    method: "get",
    params: query,
  });
}

// 活动流水导出接口
export function exportOrderPaymentRecord(query) {
  return request({
    url: "/order/orderPaymentRecord/export",
    method: "get",
    params: query,
  });
}

// 活动流水详情
export function getOrderPaymentRecordById(query) {
  return request({
    url: `/order/orderPaymentRecord/${query.id}`,
    method: "get",
  });
}
