import request from "@/utils/request";

// 活动订单列表
export function getActivityList(query) {
  return request({
    url: "/activity/activity/list",
    method: "get",
    params: query,
  });
}

// 导出活动订单
export function exportActivity(query) {
  return request({
    url: "/activity/activity/export",
    method: "get",
    params: query,
  });
}

// 活动订单退款
export function refundActivity(data) {
  return request({
    url: "/activity/activity/refund",
    method: "put",
    data: data,
  });
}

// 获取活动订单详情
export function getActivityById(query) {
  return request({
    url: `/activity/activity/${query.id}`,
    method: "get",
  });
}
