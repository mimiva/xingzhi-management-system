import request from "@/utils/request";

// 核销列表
export function getWriteOffList(query) {
  return request({
    url: "/writeOff/log/list",
    method: "get",
    params: query,
  });
}

// 快速核销
export function writeOff(data) {
  return request({
    url: `/writeOff/log`,
    method: "post",
    data: data,
  });
}

// 核销详情
export function getWriteOffById(query) {
  return request({
    url: `/writeOff/log/${query.id}`,
    method: "get",
  });
}

// 导出活动订单
export function exportWriteOffList(query) {
  return request({
    url: "/writeOff/log/export",
    method: "get",
    params: query,
  });
}
