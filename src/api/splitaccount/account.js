import request from '@/utils/request'

// 查询分账列表
export function listAccount(query) {
  return request({
    url: '/splitaccount/account/list',
    method: 'get',
    params: query
  })
}

// 查询分账详细
export function getAccount(splitAccountId) {
  return request({
    url: '/splitaccount/account/' + splitAccountId,
    method: 'get'
  })
}

// 新增分账
export function addAccount(data) {
  return request({
    url: '/splitaccount/account',
    method: 'post',
    data: data
  })
}

// 余额查询
export function altMchQuery(data) {
  return request({
    url: '/splitaccount/account/balanceQuery?altMchNo='+data,
    method: 'get'
  })
}

// 余额提现
export function altMchWithdrawal(altMchNo) {
  const data = {
    altMchNo
  }
  return request({
    url: '/splitaccount/account/balanceWithdrawal',
    method: 'post',
    data: data
  })
}

// 修改分账
export function updateAccount(data) {
  return request({
    url: '/splitaccount/account',
    method: 'put',
    data: data
  })
}

// 删除分账
export function delAccount(splitAccountId) {
  return request({
    url: '/splitaccount/account/' + splitAccountId,
    method: 'delete'
  })
}

// 导出分账
export function exportAccount(query) {
  return request({
    url: '/splitaccount/account/export',
    method: 'get',
    params: query
  })
}


export function changeSplitAccountStatus(splitAccountId, splitAccountStatus) {
  const data = {
    splitAccountStatus,
    splitAccountId
  }
  return request({
    url: '/splitaccount/account/changeStatus',
    method: 'put',
    data: data
  })
}



export function sign(splitAccountId) {
  const data = {
    splitAccountId
  }
  return request({
    url: '/splitaccount/account/sign',
    method: 'post',
    data:data
  })
}


export function auth(splitAccountId) {
  const data = {
    splitAccountId
  }
  return request({
    url: '/splitaccount/account/auth',
    method: 'post',
    data:data
  })
}
