import request from '@/utils/request'

// 获取秒杀活动列表
export function getSeckillList(query) {
  return request({
    url: '',
    method: 'get',
    params: query
  })
}
