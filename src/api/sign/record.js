import request from '@/utils/request'

// 查询签约记录列表
export function listRecord(query) {
  return request({
    url: '/signRecord/record/list',
    method: 'get',
    params: query
  })
}

export function unsign(query) {
  return request({
    url: '/signRecord/record/unsign',
    method: 'get',
    params: query
  })
}
export function listLog(query) {
  return request({
    url: '/signLog/list',
    method: 'get',
    params: query
  })
}

// 导出签约记录
export function exportRecord(query) {
  return request({
    url: '/signRecord/record/export',
    method: 'get',
    params: query
  })
}
