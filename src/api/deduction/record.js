import request from '@/utils/request'

// 查询扣款请求日志列表
export function listRecord(query) {
  return request({
    url: '/deduction/order/record/list',
    method: 'get',
    params: query
  })
}

// 查询扣款请求日志详细
export function getRecord(deductionOrderRecordId) {
  return request({
    url: '/deduction/order/record/' + deductionOrderRecordId,
    method: 'get'
  })
}

// 新增扣款请求日志
export function addRecord(data) {
  return request({
    url: '/deduction/order/record',
    method: 'post',
    data: data
  })
}

// 修改扣款请求日志
export function updateRecord(data) {
  return request({
    url: '/deduction/order/record',
    method: 'put',
    data: data
  })
}

// 删除扣款请求日志
export function delRecord(deductionOrderRecordId) {
  return request({
    url: '/deduction/order/record/' + deductionOrderRecordId,
    method: 'delete'
  })
}

// 导出扣款请求日志
export function exportRecord(query) {
  return request({
    url: '/deduction/order/record/export',
    method: 'get',
    params: query
  })
}
