import request from '@/utils/request'

// 查询扣款分账订单列表
export function listOrder(query) {
  return request({
    url: '/deduction/alt/order/list',
    method: 'get',
    params: query
  })
}

// 查询扣款分账订单详细
export function getOrder(deductionAltOrderId) {
  return request({
    url: '/deduction/alt/order/' + deductionAltOrderId,
    method: 'get'
  })
}

// 新增扣款分账订单
export function addOrder(data) {
  return request({
    url: '/deduction/alt/order',
    method: 'post',
    data: data
  })
}

// 修改扣款分账订单
export function updateOrder(data) {
  return request({
    url: '/deduction/alt/order',
    method: 'put',
    data: data
  })
}

// 删除扣款分账订单
export function delOrder(deductionAltOrderId) {
  return request({
    url: '/deduction/alt/order/' + deductionAltOrderId,
    method: 'delete'
  })
}

// 导出扣款分账订单
export function exportOrder(query) {
  return request({
    url: '/deduction/alt/order/export',
    method: 'get',
    params: query
  })
}
