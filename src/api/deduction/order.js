import request from '@/utils/request'

// 查询扣款订单列表
export function listOrder(query) {
  return request({
    url: '/deduction/order/list',
    method: 'get',
    params: query
  })
}

// 查询扣款订单详细
export function getOrder(deductionOrderId) {
  return request({
    url: '/deduction/order/' + deductionOrderId,
    method: 'get'
  })
}

// 新增扣款订单
export function addOrder(data) {
  return request({
    url: '/deduction/order',
    method: 'post',
    data: data
  })
}

// 修改扣款订单
export function updateOrder(data) {
  return request({
    url: '/deduction/order',
    method: 'put',
    data: data
  })
}

// 删除扣款订单
export function delOrder(deductionOrderId) {
  return request({
    url: '/deduction/order/' + deductionOrderId,
    method: 'delete'
  })
}

// 导出扣款订单
export function exportOrder(query) {
  return request({
    url: '/deduction/order/export',
    method: 'get',
    params: query
  })
}