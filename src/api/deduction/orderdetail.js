import request from '@/utils/request'

// 查询扣款请求明细日志列表
export function listRecord(query) {
  return request({
    url: '/deduction/order/detail/record/list',
    method: 'get',
    params: query
  })
}

// 查询扣款请求明细日志详细
export function getRecord(deductionOrderDetailRecordId) {
  return request({
    url: '/deduction/order/detail/record/' + deductionOrderDetailRecordId,
    method: 'get'
  })
}

// 新增扣款请求明细日志
export function addRecord(data) {
  return request({
    url: '/deduction/order/detail/record',
    method: 'post',
    data: data
  })
}

// 修改扣款请求明细日志
export function updateRecord(data) {
  return request({
    url: '/deduction/order/detail/record',
    method: 'put',
    data: data
  })
}

// 删除扣款请求明细日志
export function delRecord(deductionOrderDetailRecordId) {
  return request({
    url: '/deduction/order/detail/record/' + deductionOrderDetailRecordId,
    method: 'delete'
  })
}

// 导出扣款请求明细日志
export function exportRecord(query) {
  return request({
    url: '/deduction/order/detail/record/export',
    method: 'get',
    params: query
  })
}
