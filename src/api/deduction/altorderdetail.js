import request from '@/utils/request'

// 查询扣款分账订单明细列表
export function listDetail(query) {
  return request({
    url: '/deduction/alt/order/detail/list',
    method: 'get',
    params: query
  })
}
