import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

/* Layout */
import Layout from "@/layout";

/**
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * meta : {
    noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
    activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
  }
 */

// 公共路由
export const constantRoutes = [
  {
    path: "/redirect",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/redirect/:path(.*)",
        component: (resolve) => require(["@/views/redirect"], resolve),
      },
    ],
  },
  {
    path: "/login",
    component: (resolve) => require(["@/views/login"], resolve),
    hidden: true,
  },
  {
    path: "/register",
    component: (resolve) => require(["@/views/register"], resolve),
    hidden: true,
  },
  {
    path: "/404",
    component: (resolve) => require(["@/views/error/404"], resolve),
    hidden: true,
  },
  {
    path: "/401",
    component: (resolve) => require(["@/views/error/401"], resolve),
    hidden: true,
  },
  {
    path: "",
    component: Layout,
    redirect: "index",
    children: [
      {
        path: "index",
        component: (resolve) => require(["@/views/index"], resolve),
        name: "Index",
        meta: { title: "首页", icon: "dashboard", affix: true },
      },
    ],
  },
  {
    path: "/user",
    component: Layout,
    hidden: true,
    redirect: "noredirect",
    children: [
      {
        path: "profile",
        component: (resolve) =>
          require(["@/views/system/user/profile/index"], resolve),
        name: "Profile",
        meta: { title: "个人中心", icon: "user" },
      },
    ],
  },
  {
    path: "/system/user-auth",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "role/:userId(\\d+)",
        component: (resolve) =>
          require(["@/views/system/user/authRole"], resolve),
        name: "AuthRole",
        meta: { title: "分配角色", activeMenu: "/system/user" },
      },
    ],
  },
  {
    path: "/system/role-auth",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "user/:roleId(\\d+)",
        component: (resolve) =>
          require(["@/views/system/role/authUser"], resolve),
        name: "AuthUser",
        meta: { title: "分配用户", activeMenu: "/system/role" },
      },
    ],
  },
  {
    path: "/system/dict-data",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "index/:dictId(\\d+)",
        component: (resolve) => require(["@/views/system/dict/data"], resolve),
        name: "Data",
        meta: { title: "字典数据", activeMenu: "/system/dict" },
      },
    ],
  },
  {
    path: "/monitor/job-log",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "index",
        component: (resolve) => require(["@/views/monitor/job/log"], resolve),
        name: "JobLog",
        meta: { title: "调度日志", activeMenu: "/monitor/job" },
      },
    ],
  },
  {
    path: "/tool/gen-edit",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "index",
        component: (resolve) =>
          require(["@/views/tool/gen/editTable"], resolve),
        name: "GenEdit",
        meta: { title: "修改生成配置", activeMenu: "/tool/gen" },
      },
    ],
  },

  // 秒杀活动业务路由
  {
    path: "/activity/seckill",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/seckill/add",
        component: (resolve) =>
          require(["@/views/activity/seckill/child/add"], resolve),
        name: "SeckillAdd",
        meta: { title: "新增秒杀活动", activeMenu: "/mk/seckill" },
      },
      {
        path: "/seckill/detail/:id",
        component: (resolve) =>
          require(["@/views/activity/seckill/child/detail"], resolve),
        name: "SeckillDetil",
        meta: { title: "查看秒杀活动", activeMenu: "/mk/seckill" },
      },
      {
        path: "/seckill/copy/:id",
        component: (resolve) =>
          require(["@/views/activity/seckill/child/add"], resolve),
        name: "SeckillCopy",
        meta: { title: "复制秒杀活动", activeMenu: "/mk/seckill" },
      },
      {
        path: "/seckill/active/:id",
        component: (resolve) =>
          require(["@/views/activity/seckill/child/active"], resolve),
        name: "SeckillActive",
        meta: { title: "查看活动数据", activeMenu: "/mk/seckill" },
      },
      {
        path: "/seckill/commission/:id",
        component: (resolve) =>
          require(["@/views/activity/seckill/child/commission"], resolve),
        name: "SeckillCommission",
        meta: { title: "员工提成数据", activeMenu: "/mk/seckill" },
      },
      {
        path: "/seckill/sub/:id",
        component: (resolve) =>
          require(["@/views/activity/seckill/child/sub"], resolve),
        name: "SeckillSub",
        meta: { title: "客户分佣数据", activeMenu: "/mk/seckill" },
      },
    ],
  },

  // 集客活动业务路由
  {
    path: "/activity/gather/add",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/gather/add/:type",
        component: (resolve) =>
          require(["@/views/activity/gather/child/add"], resolve),
        name: "GatherAdd",
        meta: { title: "新增集客活动", activeMenu: "/mk/gather" },
      },
      {
        path: "/gather/detail/:id",
        component: (resolve) =>
          require(["@/views/activity/gather/child/detail"], resolve),
        name: "GatherDetil",
        meta: { title: "查看集客活动", activeMenu: "/mk/gather" },
      },
      {
        path: "/gather/copy/:id/:type",
        component: (resolve) =>
          require(["@/views/activity/gather/child/add"], resolve),
        name: "GatherCopy",
        meta: { title: "复制集客活动", activeMenu: "/mk/gather" },
      },
      {
        path: "/gather/active/:id",
        component: (resolve) =>
          require(["@/views/activity/gather/child/active"], resolve),
        name: "GatherActive",
        meta: { title: "查看活动数据", activeMenu: "/mk/gather" },
      },
    ],
  },

  // 同行活动业务路由
  {
    path: "/activity/peer/add",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/peer/add/:type",
        component: (resolve) =>
          require(["@/views/activity/peer/child/add"], resolve),
        name: "PeerAdd",
        meta: { title: "新增同行活动", activeMenu: "/mk/peer" },
      },
      {
        path: "/peer/detail/:id",
        component: (resolve) =>
          require(["@/views/activity/peer/child/detail"], resolve),
        name: "PeerDetil",
        meta: { title: "查看同行活动", activeMenu: "/mk/peer" },
      },
      {
        path: "/peer/copy/:id/:type",
        component: (resolve) =>
          require(["@/views/activity/peer/child/add"], resolve),
        name: "PeerCopy",
        meta: { title: "复制同行活动", activeMenu: "/mk/peer" },
      },
      {
        path: "/peer/active/:id",
        component: (resolve) =>
          require(["@/views/activity/peer/child/active"], resolve),
        name: "PeerActive",
        meta: { title: "查看活动数据", activeMenu: "/mk/peer" },
      },
    ],
  },

  // 抽奖活动业务路由
  {
    path: "/activity/luckdraw/add",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/luckdraw/add/:type",
        component: (resolve) =>
          require(["@/views/activity/luckdraw/child/add"], resolve),
        name: "LuckdrawAdd",
        meta: { title: "新增抽奖活动", activeMenu: "/mk/luckdraw" },
      },
      {
        path: "/luckdraw/detail/:id",
        component: (resolve) =>
          require(["@/views/activity/luckdraw/child/detail"], resolve),
        name: "LuckdrawDetil",
        meta: { title: "查看抽奖活动", activeMenu: "/mk/luckdraw" },
      },
      {
        path: "/luckdraw/copy/:id/:type",
        component: (resolve) =>
          require(["@/views/activity/luckdraw/child/add"], resolve),
        name: "LuckdrawCopy",
        meta: { title: "复制抽奖活动", activeMenu: "/mk/luckdraw" },
      },
      {
        path: "/luckdraw/active/:id",
        component: (resolve) =>
          require(["@/views/activity/luckdraw/child/active"], resolve),
        name: "LuckdrawActive",
        meta: { title: "查看活动数据", activeMenu: "/mk/luckdraw" },
      },
    ],
  },

  // 优惠券管理路由
  {
    path: "/coupon/manage",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/coupon/manage/add",
        component: (resolve) =>
          require(["@/views/coupon/manage/child/add"], resolve),
        name: "CouponAdd",
        meta: { title: "新增优惠券", activeMenu: "/mk/manage" },
      },
      {
        path: "/coupon/manage/edit/:id",
        component: (resolve) =>
          require(["@/views/coupon/manage/child/add"], resolve),
        name: "CouponEdit",
        meta: { title: "编辑优惠券", activeMenu: "/mk/manage" },
      },
      {
        path: "/coupon/manage/detail/:id",
        component: (resolve) =>
          require(["@/views/coupon/manage/child/detail"], resolve),
        name: "LuckdrawDetil",
        meta: { title: "查看优惠券", activeMenu: "/mk/manage" },
      },
      {
        path: "/coupon/manage/appoint/:id",
        component: (resolve) =>
          require(["@/views/coupon/manage/child/appoint"], resolve),
        name: "LuckdrawAppoint",
        meta: { title: "指定业务", activeMenu: "/mk/manage" },
      },
    ],
  },

  // 优惠券业务路由
  {
    path: "/coupon/business",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/coupon/business/edit/:id",
        component: (resolve) =>
          require(["@/views/coupon/business/child/add"], resolve),
        name: "BusinessEdit",
        meta: { title: "编辑优惠券业务", activeMenu: "/mk/business" },
      },
      {
        path: "/coupon/business/detail/:id",
        component: (resolve) =>
          require(["@/views/coupon/business/child/detail"], resolve),
        name: "BusinessDetil",
        meta: { title: "查看优惠券业务", activeMenu: "/mk/business" },
      },
      {
        path: "/coupon/business/active/:id",
        component: (resolve) =>
          require(["@/views/coupon/business/child/active"], resolve),
        name: "BusinessActive",
        meta: { title: "查看活动数据", activeMenu: "/mk/business" },
      },
    ],
  },

  // 客户优惠券路由
  {
    path: "/coupon/member",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/coupon/member/detail/:id",
        component: (resolve) =>
          require(["@/views/coupon/member/child/detail"], resolve),
        name: "LuckdrawDetil",
        meta: { title: "查看客户优惠券", activeMenu: "/mk/member" },
      },
    ],
  },

  // 活动订单
  {
    path: "/order",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/order/activity/detail/:id",
        component: (resolve) =>
          require(["@/views/order/activity/child/detail"], resolve),
        name: "ActivityDetil",
        meta: { title: "查看活动订单详情", activeMenu: "/order/orderActivity" },
      },
      {
        path: "/order/writeoff/detail/:id",
        component: (resolve) =>
          require(["@/views/order/writeoff/child/detail"], resolve),
        name: "WriteoffDetil",
        meta: {
          title: "查看活动订单核销详情",
          activeMenu: "/order/orderWriteoff",
        },
      },
    ],
  },
  // 活动订单
  {
    path: "/finance",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/finance/activity/detail/:id",
        component: (resolve) =>
          require(["@/views/finance/activity/child/detail"], resolve),
        name: "ActivityDetil",
        meta: { title: "查看活动流水详情", activeMenu: "/finance/activity" },
      },
    ],
  },
  {
    path: "/deduction/secondDetail",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "record/:deductionOrderId(\\d+)",
        component: (resolve) =>
          require(["@/views/deduction/record/index"], resolve),
        name: "DeductionOrderRecord",
        meta: { title: "扣款详情", activeMenu: "/deduction/record" },
      },
    ],
  },
  {
    path: "/deduction/threeDetail",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "orderDetail/:deductionOrderRecordId(\\d+)",
        component: (resolve) =>
          require(["@/views/deduction/orderdetail/index"], resolve),
        name: "DeductionOrderDetailRecord",
        meta: { title: "扣款明细", activeMenu: "/deduction/orderdetail" },
      },
    ],
  },
  {
    path: "/deduction/altSecondDetail",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "altOrderDetail/:deductionAltOrderId(\\d+)",
        component: (resolve) =>
          require(["@/views/deduction/altorderdetail/index"], resolve),
        name: "DeductionAltOrderDetailRecord",
        meta: { title: "分账明细", activeMenu: "/deduction/altorderdetail" },
      },
    ],
  },
];

export default new Router({
  // mode: "history", // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes,
});
