/**
 * 自定义防止点击事件
 */

/**
 * v-dialogDragWidth 可拖动弹窗宽度（右侧边）
 * Copyright (c) 2019 ruoyi
 */

export default {
  bind(el, binding, vnode, oldVnode) {
    el.addEventListener("click", () => {
      if (!el.disabled) {
        el.disabled = true;
        // 传入绑定值就使用，默认3000毫秒内不可重复点击
        setTimeout(() => (el.disabled = false), 3000);
      }
    });
  },
};
